const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');

const PORT = process.env.PORT || 3007;
const app = express();

//connect our routes module
const userRoutes = require(`./routes/userRoutes`);
const courseRoutes = require(`./routes/courseRoutes`);

//middleware to handle JSON payloads
app.use(express.json());
app.use(express.urlencoded({extended:true}));


//CORS -prevents blocking of requests from frontend(client) esp different domains
app.use(cors());

//connect Database to server
//mongoose.connect(process.env, MONGO_URL, {useNewURlParser:true, useUnifiedTopology:true});
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser:true, useUnifiedTopology:true});

//test DB connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));

//Schema
    //schema is in models module

//Routes
    //create a middleware to be the root url of all routes
app.use(`/api/users`, userRoutes);
app.use(`/api/courses`, courseRoutes);

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))