const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, `First name is required`]
    },
    lastName: {
        type: String,
        required: [true, `Last name is required`]
    },
    email: {
        type: String,
        required: [true, `Email name is required`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `Password name is required`]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, `Course ID is required`]
            },
            status: {
                type: String,
                default: "Enrolled"
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]

}, {timestamps: true});

module.exports  = mongoose.model(`User`, userSchema);