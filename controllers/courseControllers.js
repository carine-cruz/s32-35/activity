const Course = require('./../models/Course');

module.exports.createCourse = async (reqBody) => {

    try{
        const newCourse = new Course({
            courseName : reqBody.courseName,
            description: reqBody.description,
            price : reqBody.price
        })
    
        return await newCourse.save().then(result=> result ? result : err)
        // return await Course.findOne({courseName:reqBody.courseName}).then((result,error)=>{
        //     //console.log(`find result`)
        //     console.log(result)
        //     if (result == null){
        //         newCourse.save().then(saveResult=>{
        //             console.log(saveResult)
        //             if (saveResult !== null){
        //                 return true
        //             } else {
        //                 return false
        //             }
        //         })
        //     } else {
        //         return { message: `Course already exists`}
        //     }
        // })
    }catch(err){
        return {message: err}
    }
}
//issue: res send after successful create

module.exports.getAllCourses = async () => {
    try{
        return await Course.find().then(result => result);
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports.getCourse = async (courseId) => {
    
    try{
        return await Course.findById(courseId).then( (result, err) =>{
            if(result){
                console.log(`result found`)
                return result
            } else {
                console.log(`no match`)
                if (result == null){
                    return {message: `Course does not exist`}
                } else {
                    return err
                }
            }
        })
    }catch(err){
        return err
    }
}

module.exports.updateCourse = async (courseId, reqBody) => {
    return await Course.findByIdAndUpdate(courseId, {$set: reqBody},{new:true}).then((result, err) => {
        if(result){
            return result
        } else {
            if (result == null){
                return {message: `Course does not exist`}
            } else {
                return err
            }
        }
    })
}

module.exports.updateToActive = async(courseId) => {
    try{
        return await Course.findByIdAndUpdate(courseId,{$set: {isOffered: false}},{new:true}).then((result, err) =>
            result? result: err)

    }catch(err){
        res.status(500).json(err)
    }
}

module.exports.updateToInactive = async(courseId) => {
    try{
        return await Course.findByIdAndUpdate(courseId,{$set: {isOffered: true}},{new:true}).then((result, err) =>
            result? result: err)

    }catch(err){
        res.status(500).json(err)
    }
}

module.exports.getActiveCourses = async() => {
    try{
        return await Course.find({isOffered: true}).then(result => result);
    }catch(err){
        return err
    }
}

module.exports.deleteCourse = async (courseId) => {
    try{
        return await Course.findByIdAndDelete(courseId).then((result,err) => {
            if (result){
                return true;
            } else {
                return false;
            }
        });
    }catch(err){
        res.status(500).json(err);
    }
}
