//const User = require('../models/User');
const res = require('express/lib/response');
const User = require('./../models/User');
const CryptoJS = require("crypto-js");
const bcrypt = require('bcrypt');
const {createToken} = require('./../auth');


//REGISTER A USER
module.exports.register = async (reqBody) => {
    //console.log(reqBody)

    const {firstName, lastName, email, password} = reqBody;

    const newUser = new User({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
        //password: bcrypt.hashSync(password, 10)
    });

    try{
        return await newUser.save().then(result => {
            if (result){
                return true
            } else {
                if (result == null){
                    return false
                }
            }
        })
    }catch(err){
        res.status(500).json(err)
    }
}

//GET ALL USERS
module.exports.getAllUsers = async () =>{
    return await User.find().then(result => result)
}

//CHECK IF EMAIL ALREADY EXISTS
module.exports.checkEmail = async (reqBody) => {
    const {email} = reqBody

    return await User.findOne({email: email}).then( (result, err) => {
        if (result){
            return true
            //with existing
        } else {
            if (result == null){
                return false
                //ok to register
            } else {
                return err
            }
        }
    })

}

//LOGIN A USER
module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if pw is correct, decrypt to compare pw input from the user from pw stored in db
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				// console.log(reqBody.password == decryptedPw); //true
                // console.log(reqBody.password);
                // console.log(decryptedPw);
		

				if(reqBody.password == decryptedPw){
					//create a token for the user
					return { token: createToken(result) }
				} else {
					return {auth: `Auth Failed!`}
				}

			} else {
				return err
			}
		}
	})
}

//RETRIEVE USER INFO
module.exports.profile = async (id) => {

    return await User.findById(id).then( (result, err) =>{
        if(result){
            return result
        } else {
            if (result == null){
                return {message: `User does not exist`}
            } else {
                return err
            }
        }
    })

}

//------ ASSIGNMENT --------------
//Update profile - assignment
module.exports.updateProfile = async (id, details) => {
   // console.log(`update profile`)

    //encrypt user password
    details.password = CryptoJS.AES.encrypt(details.password, process.env.SECRET_PASS).toString();

    return await User.findByIdAndUpdate(id, {$set: details},{new:true}).then((result, err) => {
        if(result){
            return result
        } else {
            if (result == null){
                return {message: `User does not exist`}
            } else {
                return err
            }
        }
    })
}

//Update password - assignment
module.exports.updatePassword = async (id, newPassword) => {

        //encrypt user password
        newPassword = CryptoJS.AES.encrypt(newPassword, process.env.SECRET_PASS).toString();

        return await User.findByIdAndUpdate(id, {$set: {"password": newPassword}},{new:true}).then((result, err) => {
            
            //successful password update
            if(result){
                result.password = "***"
                //return true
                return result
            //error
            } else {
                //return false;
                return err
            }
        })
}

//delete user - assignment
module.exports.deleteUser = async(reqBody) => {
    //console.log(reqBody.email)
    try{
        return await User.findOneAndDelete({email: reqBody.email}).then((result,err) => {
            if (result){
                return true;
            } else {
                return false;
            }
        });
    }catch(err){
        res.status(500).json(err);
    }
}
//update admin status
module.exports.updateAdminStatus = async(reqBody) => {
    try{
        // let userAdmin = User.findById(id, {isAdmin:1}).then(result=>console.log(result))
        // console.log('userAdmin')
        // console.log(userAdmin)

        const {email} = reqBody

        return await User.findOneAndUpdate({email: email},{$set: {isAdmin: true}}).then((result, err) =>
            result ? true : err)

    }catch(err){
        res.status(500).json(err)
    }
}

//update status to user
module.exports.updateUserStatus = async(reqBody) => {
    try{

        return await User.findOneAndUpdate({email: reqBody.email},{$set: {isAdmin: false}}).then((result, err) => result ? true : err)

    }catch(err){
        res.status(500).json(err)
    }
}


//UPDATE USER INFO
module.exports.update = async (userId, reqBody) => {
    
    const userData = {
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }

    return await User.findByIdAndUpdate(userId, {$set: userData}, {new: true}).then((result, err) => {
        if (result){
            result.password = "***"
            return result
        } else {
            return err
        }
    } )

}