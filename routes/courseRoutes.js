const express = require('express');
const res = require('express/lib/response');
const { verifyAdmin, verify } = require('../auth');
const router = express.Router();

const { createCourse, 
        getAllCourses, 
        getCourse,
        updateCourse,
        updateToActive,
        updateToInactive,
        getActiveCourses,
        deleteCourse
} = require('./../controllers/courseControllers')

//Create a route /create to create a new course, save the course in DB and return the document
	//Only admin can create a course
router.post(`/create`, verifyAdmin, async (req, res) => {
    
    try{
        await createCourse(req.body).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err)
    }
})

//Create a route "/" to get all the courses, return all documents found
	//both admin and regular user can access this route
router.get(`/`, verify, async (req, res) => {
    try{
        await getAllCourses().then(result=>res.send(result))
    }catch(err){
        console.log(`error`)
        res.status(500).json(err);
    }
} )


//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
	//both admin and regular user can access this route
    
router.get(`/:courseId`, verify, async(req, res) => {

    try{
        await getCourse(req.params.courseId).then(result=>res.send(result))
    }catch(err){
        res.status(500).send(err)
    }
} )


//Create a route "/:courseId/update" to update course info, return the updated document
	//Only admin can update a course
router.put(`/:courseId/update`, verifyAdmin, async (req, res) => {
    try{
        await updateCourse(req.params.courseId,req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err);
    }
})


//Create a route "/:courseId/archive" to update isActive to false, return updated document
	//Only admin can update a course
router.patch(`/:courseId/archive`, verifyAdmin, async (req, res) => {
   
    try{
        await updateToActive(req.params.courseId).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})


//Create a route "/:courseId/unArchive" to update isActive to true, return updated document
	//Only admin can update a course
router.patch(`/:courseId/unarchive`, verifyAdmin, async (req, res) => {
    try{
        await updateToInactive(req.params.courseId).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})


//Create a route "/isActive" to get all active courses, return all documents found
	//both admin and regular user can access this route
router.get(`/isActive`, verify, async (req, res) => {
    try{
        console.log(`hello`);
        await getActiveCourses().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})


//Create a route "/:id/delete-course" to delete a courses, return true if success
	//Only admin can delete a course
router.delete(`/:courseId/delete-course`, verifyAdmin, async (req, res) => {
    try{
        await deleteCourse(req.params.courseId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

module.exports = router;