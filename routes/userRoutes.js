const express = require('express');
const router = express.Router();

//import units of functions from user controller module
const {
    register, 
    getAllUsers,
    checkEmail,
    login,
    profile,
    updateProfile,
    updatePassword,
    deleteUser,
    update,
    updateAdminStatus,
    updateUserStatus
} = require(`./../controllers/userController`);

const {verify, decode, verifyAdmin} = require('./../auth');


//GET ALL USERS
router.get('/', async (req, res) => {
    try{
       await getAllUsers().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err);
    }
})

//REGISTER A USER
router.post('/register', async (req, res) => {
    //console.log(req.body) //user object

    try{
        await register(req.body).then(result => {

            res.send(result)
        })
    } catch(err){
        res.status(500).json(err)
    }
})

//CHECK IF EMAIL ALREADY EXISTS
router.post('/email-exists', async (req, res) => {
    try{
       await checkEmail(req.body).then(result => res.send(result));
    }catch(error){
        res.status(500).json(error)
    }
})

//LOGIN THE USER
    // authentication
router.post('/login', (req, res) => {
    //console.log(req.body)

    try{
        login(req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//GET SPECIFIC USER
router.get('/profile', verify, async (req, res) => {

    //res.send(`welcome to get request`);
    const userId = decode(req.headers.authorization).id;
    //console.log(userId);

    try{
        
        await profile(userId).then(result=>res.send(result));
    
    }catch(err){
        res.status(500).json(err);
    }
})

//create route to update user information, make sure the route is secure with token
//UPDATE PROFILE - ASSIGNMENT
// router.put('/updateProfile', verify, async (req, res) =>{
//     const userId = decode(req.headers.authorization).id;


//     try{
        
//         await updateProfile(userId, req.body).then(result=>res.send(result));
    
//     }catch(err){
//         res.status(500).json(err);
//     }
// })

//router.put('/:id/update)
router.put('/update', verify, async (req, res) =>{
    const userId =  decode(req.headers.authorization).id

    try{
        await update(userId, req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//create a route a route to update user's password, make sure the route is secured with token
//UPDATE PASSWORD - ASSIGNMENT
//can use router.patch
router.put('/update-password', verify, async (req, res) => {
    const userId = decode(req.headers.authorization).id;

    try{
        await updatePassword(userId, req.body.password).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err);
    }
})

//create a route to delete a user from the database, make sure the route is secured with token
    //stretch goal, only admin can delete a user
//DELETE PASSWORD - assignment
router.delete('/delete-user', verifyAdmin, async (req, res) => {

    try{
        deleteUser(req.body).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }

})

//Update isAdmin status
router.patch('/isAdmin', verifyAdmin, async (req, res) => {
    // const admin = decode(req.headers.authorization).isAdmin

    //FIRST METHOD (manual decode and check token)

    // try{
    //     if(admin == true){
    //         await updateAdminStatus(req.body).then(result => res.send(result))
    //     } else {
    //         res.send(`You are not authorized`)
    //     }
    // }catch(err){
    //     res.status(500).json(err)
    // }

    //USING VERIFY ADMIN:
    try{
        await updateAdminStatus(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

//CHANGE TO USER STATUS
router.patch('/isUser', verifyAdmin, async (req, res) => {
    try{
        await updateUserStatus(req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//export the router module to be used in index.js file
module.exports = router;